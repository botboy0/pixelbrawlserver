package game;

import app.PixelBrawlServer;
import model.Player.Player;

public class Character {

private Player player;
private String attack;
private float hpmax;
private float hp;
public Character(Player player) {
	this.player=player;
	}


public void Attack(game.Character target) {
	PixelBrawlServer.server.log(player.getUsername()+" attacked "+target.getPlayer().getUsername()+" with "+getAttack());
	if (player==player.getMatch().getPlayer()) {
		
		player.getConn().send("match attack enemy "+getAttack()+" "+player.getLevel(attack));
		player.getMatch().getEnemy().getConn().send("match attack enemy "+getAttack()+" "+player.getLevel(attack));
	}
	else {
		player.getConn().send("match attack player "+getAttack()+" "+player.getLevel(attack));
		player.getMatch().getPlayer().getConn().send("match attack player "+getAttack()+" "+player.getLevel(attack));
		
	} 
}

public String getAttack() {
	return attack;
}

public void setAttack(String attack) {
	this.attack = attack;
}


public Player getPlayer() {
	return player;
}


public float getHp() {
	return hp;
}


public void setHp(float hp) {
	this.hp = hp;
}


}
