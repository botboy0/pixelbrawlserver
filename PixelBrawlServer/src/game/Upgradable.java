package game;

public enum Upgradable {
	ROCK(      0,
			   new float[]{50,75,100,150,200},
		       null,
		       5,
		       new float[]{25,50,75,100},
		       new float[]{100,50,25,10}),
	
	
	
	PAPER(     1,
			   new float[]{50,100,50,100,15},
			   new int[]{2,2,2,3,2},
			   5,
			   new float[]{25,50,75,100},
			   new float[]{100,50,25,10}),
	
	
	
	SCISSORS(  2,
			   new float[]{50,75,100,150,200},
			   null,
			   5,
			   new float[]{25,50,75,100},
			   new float[]{100,50,25,10}),
	
	
	
	ARMOR(     3,
			   new float[]{5,10,15,20,25,30,35,40},
			   null,
			   8,
			   new float[]{25,50,75,100,125,150,200},
			   new float[]{100,50,30,20,15,10,5}),
	
	
	
	HEALTH(    4,
			   new float[]{100,125,150,175,200,250,300,400},
			   null,
			   8,
			   new float[]{25,50,75,100,125,150,200},
			   new float[]{100,50,30,20,15,10,5}),
	
	
	
	MAGICRESIST(  5,
			   new float[]{5,10,15,20,25,30,35,40},
			   null,
			   8,
			   new float[]{25,50,75,100,125,150,200},
			   new float[]{100,50,30,20,15,10,5});
	
	
	private final int number;
	private final float[] stats;
	private final int[] durations;
	private final int maxlevel;
	private final float[] costs;
	private final float[] chances;
	
	
	Upgradable(int number,float[]stats,int[]durations,int maxlevel,float[]costs,float[]chances) {
		this.number=number;
		this.stats=stats;
		this.durations=durations;
		this.maxlevel=maxlevel;
		this.costs=costs;
		this.chances=chances;
	}


	public static Upgradable getStat(int number) {
		switch (number) {
		case 0:	
			return ROCK;
		case 1:	
			return PAPER;
		case 2:	
			return SCISSORS;	
		case 3:		
			return ARMOR;
		case 4:	
			return HEALTH;
		case 5:	
			return MAGICRESIST;
		default:
			return null;
		}
	}
	
	public int getNumber() {
		return number;
	}


	public float[] getStats() {
		return stats;
	}


	public int[] getDurations() {
		return durations;
	}


	public int getMaxlevel() {
		return maxlevel;
	}


	public float[] getCosts() {
		return costs;
	}


	public float[] getChances() {
		return chances;
	}
	
	
	
}
